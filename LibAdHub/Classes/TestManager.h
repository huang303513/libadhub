//
//  TestManager.h
//  LibAdHub
//
//  Created by huangchengdu on 2018/10/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^AdHubLoadCompletion)(BOOL success);

@interface TestManager : NSObject

/*
 * appID  Adhub对应的应用ID
 * spaceID 广告ID
 * fromViewController 点击广告以后跳转的来源VC
 */
@property(nonatomic, strong)NSDictionary *sdkConfig;

+(instancetype)sharedManager;
-(void)startWithCompletion:(AdHubLoadCompletion)block;
@end

NS_ASSUME_NONNULL_END
