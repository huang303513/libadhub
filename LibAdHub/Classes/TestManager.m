//
//  TestManager.m
//  LibAdHub
//
//  Created by huangchengdu on 2018/10/23.
//


#import "TestManager.h"
#import <AdHubSDK/AdHubSDK.h>


@interface TestManager ()<AdHubSplashDelegate>

@property(nonatomic, strong)NSString *appID;
@property(nonatomic, strong)NSString *spaceID;
@property(nonatomic, strong)UIViewController *fromViewController;
@property(nonatomic, assign)NSInteger timeout;

@property(nonatomic, assign)BOOL hasTimeOuted;

@property(nonatomic, strong)AdHubSplash *splash;
@property(nonatomic, strong)NSTimer *timer;
@property(nonatomic, copy)AdHubLoadCompletion loadBlock;

@end

@implementation TestManager

+(instancetype)sharedManager {
    static dispatch_once_t onceToken;
    static TestManager *_sharedManager = nil;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[TestManager alloc] init];
    });
    return _sharedManager;
}

- (void)setSdkConfig:(NSDictionary *)sdkConfig{
    _sdkConfig = sdkConfig;
    self.appID = [sdkConfig valueForKey:@"appID"];
    self.spaceID = [sdkConfig valueForKey:@"spaceID"];
    self.fromViewController = [sdkConfig valueForKey:@"fromViewController"];
    self.timeout = [[sdkConfig valueForKey:@"timeout"] integerValue];
}



- (void)startWithCompletion:(AdHubLoadCompletion)block{
    
    if (!(self.appID && self.spaceID && self.fromViewController  && self.timeout)) {
        block(NO);
        return;
    }
    self.loadBlock = block;
    self.hasTimeOuted = NO;
    [AdHubSDKManager configureWithApplicationID:self.appID];
    self.splash = [[AdHubSplash alloc] initWithSpaceID:self.spaceID spaceParam:@""];
    self.splash.delegate = self;
    [self.splash loadAndDisplayUsingKeyWindow:[UIApplication sharedApplication].keyWindow];
}







- (UIViewController *)adSplashViewControllerForPresentingModalView{
    return self.fromViewController;
}
/**
 开屏请求成功
 */
- (void)splashDidReceiveAd:(AdHubSplash *)ad{
   
    
}

/**
 开屏请求失败
 */
- (void)splash:(AdHubSplash *)ad didFailToLoadAdWithError:(AdHubRequestError *)error{
    
}

/**
 开屏展现
 */
- (void)splashDidPresentScreen:(AdHubSplash *)ad{
   
    
}

/**
 开屏点击 landingPageURL 为空时说明有详情页
 点击广告, 手动调用 splashCloseAd 会移除当前广告展示
 */
- (void)splashDidClick:(NSString *)landingPageURL{
    
}

/**
 开屏消失
 */
- (void)splashDidDismissScreen:(AdHubSplash *)ad{
    
}

@end
