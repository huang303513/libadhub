//
//  main.m
//  LibAdHub
//
//  Created by huang303513 on 10/22/2018.
//  Copyright (c) 2018 huang303513. All rights reserved.
//

@import UIKit;
#import "HCDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HCDAppDelegate class]));
    }
}
